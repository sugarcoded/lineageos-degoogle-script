#/bin/bash
READ='\033[1;32;33mNow download the correct Bromite SystemWebView from https://www.bromite.org/system_web_view and save it in the Downloads folder on your Android device. Make sure to download the correct one, probably either arm or arm64. Next, read lines 24, 29 and 52 in this script. Optionally read line 14 if you are planning on replacing any of the default SMS, camera, browser, calculator or music apps. Once ALL OF THIS IS COMPLETE, comment out line 11.\033[0m'


adb root
adb remount
echo -e '\033[1;32;34mDevice connected and mounted\033[0m'
adb shell pm list users


echo -e $READ && exit #If you have run this script once, read the error, and followed the directions, comment out this line.


#Remove Google tracking apps from default profile. Commented out lines are just my preference.
#adb shell pm uninstall -k --user 0 org.lineageos.eleven
#adb shell pm uninstall -k --user 0 org.lineageos.snap
#adb shell pm uninstall -k --user 0 com.android.calculator2
#adb shell pm uninstall -k --user 0 org.lineageos.jelly
#adb shell pm uninstall -k --user 0 com.android.messaging
adb shell pm uninstall -k --user 0 com.android.hotwordenrollment.xgoogle
adb shell pm uninstall -k --user 0 com.android.hotwordenrollment.okgoogle
adb shell pm uninstall -k --user 0 com.android.contacts

#Removing the default dialer/phone app (a Google app) will keep you from using Visual Voicemail and getting notifications for new voicemails. This is because there aren't any FOSS alternatives. If you remove it you will have to manually dial *NUMBER-FOR-YOUR-SPECIFIC-CARRIER to check for, and listen to your voicemails. I recommended you leave it installed. Once you have decided, uncomment to remove the Google dialer app or leave it alone to keep it.
#adb shell pm uninstall -k --user 0 com.android.dialer
echo -e '\033[1;32;34mRemoved junk from user profile\033[0m'


#Remove junk from the work profile. If you don't have a work profile and are the only user on your device, remove or comment out every line in this block. Mine looked like this (the first number is the ID, in this case mine was 10): UserInfo{10:Work profile:1030}. Now change it in the code below. If you have multiple accounts copy and paste this chunk as many times as you need but remember to change the ID for each. Commented out lines are just my preference except for line 38. In this case you don't need to do anything to line 38 if it is just you and a work profile. This is because the android dialer app isn't installed a second time for a work profile.
#adb shell pm uninstall -k --user 10 org.lineageos.eleven
#adb shell pm uninstall -k --user 10 org.lineageos.snap
#adb shell pm uninstall -k --user 10 com.android.calculator2
#adb shell pm uninstall -k --user 10 org.lineageos.jelly
#adb shell pm uninstall -k --user 10 com.android.messaging
adb shell pm uninstall -k --user 10 com.android.hotwordenrollment.xgoogle
adb shell pm uninstall -k --user 10 com.android.hotwordenrollment.okgoogle
adb shell pm uninstall -k --user 10 com.android.contacts
#adb shell pm uninstall -k --user 0 com.android.dialer
echo -e '\033[1;32;34mRemoved junk from work profile\033[0m'


#Change captive portal urls
adb shell settings put global captive_portal_http_url http://captiveportal.kuketz.de
adb shell settings put global captive_portal_https_url https://captiveportal.kuketz.de
adb shell settings put global captive_portal_fallback_url http://captiveportal.kuketz.de
adb shell settings put global captive_portal_other_fallback_urls http://captiveportal.kuketz.de
echo -e '\033[1;32;34mChanged captive portal urls to kuketz.de\033[0m'


#Install Bromite system webview
adb shell mkdir /system/app/bromite
#Uncomment arm OR arm64 depending on your device. After this is complete, comment out line 11.
#adb shell cp /data/media/0/Download/arm_SystemWebView.apk /system/app/bromite/bromite.apk
#adb shell cp /data/media/0/Download/arm64_SystemWebView.apk /system/app/bromite/bromite.apk
adb shell chmod 644 /system/app/bromite/bromite.apk
echo -e '\033[1;32;34mBromite webview installed\033[0m'


#Backup, find and replace google supple data url with vodafone url in gps.conf
adb shell cp /vendor/etc/gps.conf /vendor/etc/BACKUP.gps.conf
adb shell sed -i 's/SUPL_HOST=supl.google.com/SUPL_HOST=supl.vodafone.com/' /vendor/etc/gps.conf
echo -e '\033[1;32;34mChanged supple data url to vodafone\033[0m'


#Change timeservers to ntp.org
adb shell settings put global ntp_server pool.ntp.org
echo -e '\033[1;32;34mChanged timeservers to pool.ntp.org\033[0m'
echo -e '\033[1;32;32mDONE! Rebooting\033[0m'
adb reboot
