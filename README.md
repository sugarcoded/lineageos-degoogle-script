This script was created using this video https://invidio.xamh.de/watch?v=ThsXFPC-_60 and this https://libreddit.kavin.rocks/r/degoogle/comments/cldohl/how_to_degoogle_lineageos_in_2019/ Reddit post. I put it in a script so you can just plug your phone in after an update and restore your config.

This was tested and works on LineageOS 18.1 on a OnePlus 7 Pro

Make sure you device is plugged in, USB debugging and rooted debugging is enabled and your device is set to file transfer.
I am not responsable for any issues this causes you or your device.
